function Copy-Installers{
    Param(
        [Parameter(Mandatory=$true, Position=0)]
        [boolean] $CopyInstallers,
        # Directory of the location the EXE's will download to
        [Parameter(Mandatory=$true, Position=1)]
        [string] $InstallerDirectory,
        # Username of the Nexus Repository account that has access to location
        [Parameter(Mandatory=$true, Position=2)]
        [string] $InstallerRepositoryUsername,
        # Password of the Installer Account, Can use environment variable or hard coded password 
        [Parameter(Mandatory=$true, Position=3)]
        [string] $InstallerRepositoryPassword,
        #The Api Installer name IE "InstallVesDesktop-Corvette_"
        [Parameter(Mandatory=$true, Position=4)]
        [string] $ApiInstallerName,
        # The name of the Web Installer 
        [Parameter(Mandatory=$true, Position=5)]
        [string] $WebInstallerName,
        #The name of the Service Installer
        [Parameter(Mandatory=$true, Position=6)]
        [string] $ServiceInstallerName,
        #The Name of the API Version IE "V2.1*"
        [Parameter(Mandatory=$true, Position=7)]
        [string] $ApiInstallerVersion,
        #The name of the Web Installer Version
        [Parameter(Mandatory=$true, Position=8)]
        [string] $WebInstallerVersion,
        #The name of the Service Installer Version
        [Parameter(Mandatory=$true, Position=9)]
        [string] $ServiceInstallerVersion

    )


  if($CopyInstallers -eq $true){
        If(!(test-path $InstallerDirectory))
        {
              New-Item -ItemType Directory -Force -Path $InstallerDirectory
        }
        $headers = @{Authorization = 'Basic ' + [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes("${InstallerRepositoryUsername}:${InstallerRepositoryPassword}"))}
        $installerRepositoryAssetSearchEndpoint = "https://artifact.prattmillerse.com/service/rest/v1/search/assets"
        $commonParams = "repository=ves2&sort=name&direction=desc"
        
        $latestApiAsset = ((Invoke-RestMethod -Uri "${installerRepositoryAssetSearchEndpoint}?${commonParams}&name=nightly/*/${ApiInstallerName}${ApiInstallerVersion}*" -Headers $headers).items | Select-Object -First 1)
        #todo:test if we got something, error if we didn't
        $ApiInstallerName = $latestApiAsset.path.split("/")[-1]
        Invoke-RestMethod -Uri $latestApiAsset.downloadUrl -Headers $headers -OutFile "${InstallerDirectory}/${ApiInstallerName}"

        $latestWebAsset = ((Invoke-RestMethod -Uri "${installerRepositoryAssetSearchEndpoint}?${commonParams}&name=nightly/*/${WebInstallerName}${WebInstallerVersion}*" -Headers $headers).items | Select-Object -First 1)
        $WebInstallerName = $latestWebAsset.path.split("/")[-1]
        Invoke-RestMethod -Uri $latestWebAsset.downloadUrl -Headers $headers -OutFile "${InstallerDirectory}/${WebInstallerName}"

        $latestServiceAsset = ((Invoke-RestMethod -Uri "${installerRepositoryAssetSearchEndpoint}?${commonParams}&name=nightly/*/${ServiceInstallerName}${ServiceInstallerVersion}*" -Headers $headers).items | Select-Object -First 1)
        $ServiceInstallerName = $latestServiceAsset.path.split("/")[-1]
        Invoke-RestMethod -Uri $latestServiceAsset.downloadUrl -Headers $headers -OutFile "${InstallerDirectory}/${ServiceInstallerName}"

    }
}


$CopyInstallerArguments = @{
    CopyInstallers = [boolean] "true"
    InstallerDirectory = "C:\temp"
    InstallerRepositoryUsername = "support"
    InstallerRepositoryPassword = ${env:InstallerRepositoryPassword}
    ApiInstallerName = "InstallVesDesktop-Corvette_"
    WebInstallerName = "InstallVesWeb_"
    ServiceInstallerName = "InstallVesServiceCorvette_"
    ApiInstallerVersion = "v2."
    WebInstallerVersion = "v2."
    ServiceInstallerVersion = "v2."
} 


Copy-Installers @CopyInstallerArguments